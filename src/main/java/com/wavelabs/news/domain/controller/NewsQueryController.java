package com.wavelabs.news.domain.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.nbos.capi.api.v0.models.RestMessage;

/**
 * 
 * @author thejasreem NewsQueryController is a class. getAllNews() method
 *         retrieves all the news from the uri(i.e from content service).
 * 
 */

@RestController
@Component
@RequestMapping(value = "/news")
public class NewsQueryController {
	static final Logger log = Logger.getLogger(NewsQueryController.class);
	@Value("${newsUrl}")
	private String newsUrl;
	@Value("${newsIdUrl}")
	private String newsIdUrl;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/all",method=RequestMethod.GET)
	public ResponseEntity getAllEvents() {
		RestTemplate restTemplate = new RestTemplate();
		RestMessage restMessage = new RestMessage();
		ResponseEntity<String> response = restTemplate.getForEntity(newsUrl, String.class);
		if (response != null) {
			return ResponseEntity.status(200).body(response.getBody());
		} else {
			restMessage.message = "404";
			restMessage.messageCode = "Retrieval of News Fails!!";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/{newsId}")
	public ResponseEntity getDealById(@PathVariable String newsId) {
		RestTemplate restTemplate = new RestTemplate();
		final String resourceURL = newsIdUrl + newsId;
		RestMessage restMessage = new RestMessage();
		ResponseEntity<String> response = restTemplate.getForEntity(resourceURL, String.class);
		if (response != null) {

			return ResponseEntity.status(200).body(response.getBody());
		} else {
			restMessage.message = "News not found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

}
