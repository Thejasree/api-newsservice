FROM java:8
ADD target/newsservice.jar newsservice.jar
ENTRYPOINT ["java","-jar","newsservice.jar"]
